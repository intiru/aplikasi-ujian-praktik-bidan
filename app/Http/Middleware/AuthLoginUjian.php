<?php

namespace app\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class AuthLoginUjian
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $login_member = Session::get('login_member');
        if (!$login_member) {
            return redirect()->route('ujianIntro');
        }

        return $next($request);
    }
}
