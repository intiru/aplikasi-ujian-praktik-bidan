<?php

namespace app\Http\Controllers\General;

use app\Models\mBidang;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Session;
use app\Models\mUser;


class Dashboard extends Controller
{
    private $breadcrumb = [
        [
            'label' => 'Dashboard',
            'route' => ''
        ]
    ];

    function index(Request $request)
    {

        $data = Main::data($this->breadcrumb);

        $total_peserta = mBidang::count();
        $total_member = mBidang::count();
        $total_ujian = mBidang::count();
        $total_soal = mBidang::count();


        $data = array_merge($data, array(
            'total_peserta' => Main::format_number($total_peserta),
            'total_member' => Main::format_number($total_member),
            'total_ujian' => Main::format_number($total_ujian),
            'total_soal' => Main::format_number($total_soal),
        ));

        return view('dashboard/dashboard_admin', $data);

    }


}
