<?php

namespace app\Http\Controllers\ManajemenSoal;

use app\Models\mBidang;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;

class Bidang extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['manajemen_soal'],
                'route' => ''
            ],
            [
                'label' => $cons['bidang'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mBidang
            ::orderBy('bdg_nama', 'ASC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('manajemen_soal/bidang/bidangList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'bdg_nama' => 'required'
        ]);

        $data = $request->except('_token');
        mBidang::create($data);
    }

    function edit_modal($id_bidang)
    {
        $id_bidang = Main::decrypt($id_bidang);
        $edit = mBidang::where('id_bidang', $id_bidang)->first();
        $data = [
            'edit' => $edit
        ];

        return view('manajemen_soal/bidang/bidangEditModal', $data);
    }

    function delete($id_bidang)
    {
        $id_bidang = Main::decrypt($id_bidang);
        mBidang::where('id_bidang', $id_bidang)->delete();
    }

    function update(Request $request, $id_bidang)
    {
        $id_bidang = Main::decrypt($id_bidang);
        $request->validate([
            'bdg_nama' => 'required'
        ]);
        $data = $request->except("_token");
        mBidang::where(['id_bidang' => $id_bidang])->update($data);
    }
}
