<?php

namespace app\Http\Controllers\App;

use app\Models\mBidang;
use app\Models\mBidangSub;
use app\Models\mMember;
use app\Models\mPeserta;
use app\Models\mPesertaJawaban;
use app\Models\mPesertaNilaiAkhir;
use app\Models\mSoal;
use app\Models\mSoalEssay;
use app\Models\mSoalKategori;
use app\Models\mUjian;
use app\Models\mUserRole;
use app\Rules\UjianEmailChecker;
use app\Rules\UjianLoginCheck;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class Ujian extends Controller
{
    private $breadcrumb;

    function __construct()
    {

    }

    function list()
    {

        $ujian = mUjian
            ::with([
                'bidang',
                'bidang_sub',
                'peserta_nilai_akhir',
                'peserta_nilai_akhir.peserta'
            ])
            ->orderBy('id_ujian', 'DESC')
            ->get();

        $data = [
            'ujian' => $ujian,
            'navigation' => [
                'tab_active' => 'ujian'
            ],
        ];
        return view('ujian/ujian/ujianList', $data);
    }

    function create()
    {
        $member = Session::get('member');
        $id_member = $member->id_member;

        $bidang = mBidang::orderBy('bdg_nama', 'ASC')->get();
        $bidang_sub = mBidangSub::where('id_bidang', $bidang[0]->id_bidang)->orderBy('bds_nama', 'ASC')->get();
        $mahasiswa = mMember
            ::leftJoin('peserta', 'peserta.id_member', '=', 'member.id_member')
            ->where('mbr_status', 'mahasiswa')
            ->orderBy('pst_nama', 'ASC')
            ->get();

        $data = [
            'bidang' => $bidang,
            'bidang_sub' => $bidang_sub,
            'peserta' => $mahasiswa,
            'navigation' => [
                'tab_active' => 'ujian'
            ]
        ];
        return view('ujian/ujian/ujianCreate', $data);
    }

    function bidang_sub(Request $request)
    {
        $id_bidang = $request->input('id_bidang');
        $bidang_sub = mBidangSub::where('id_bidang', $id_bidang)->orderBy('bds_nama', 'ASC')->get();

        $data = [
            'bidang_sub' => $bidang_sub
        ];

        return view('ujian/ujian/ujianCreateBidangSub', $data)->render();

//        return $html;
    }

    function insert(Request $request)
    {
        $request->validate([
            'id_bidang' => 'required',
            'id_bidang_sub' => 'required',
            'id_peserta' => 'required'
        ]);

        $member = Session::get('member');
        $id_member = $member->id_member;
        $id_bidang = $request->input('id_bidang');
        $id_bidang_sub = $request->input('id_bidang_sub');
        $id_peserta_arr = $request->input('id_peserta');



        $ujian_data = [
            'id_member' => $id_member,
            'id_bidang' => $id_bidang,
            'id_bidang_sub' => $id_bidang_sub,
            'id_peserta_json' => json_encode($id_peserta_arr),
            'ujn_tanggal' => date('Y-m-d')
        ];

        $ujian_response = mUjian::create($ujian_data);
        $id_ujian = $ujian_response->id_ujian;

//        return response($ujian_data,404);

        return [
            'redirect' => route('ujianPaper', ['id_ujian' => Main::encrypt($id_ujian)])
        ];
    }

    /**
     * Untuk membuat halaman soal yang akan diisi oleh penguji
     *
     * @param $id_ujian
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function paper_page($id_ujian)
    {
        $id_ujian = Main::decrypt($id_ujian);
        $ujian = mUjian::where('id_ujian', $id_ujian)->first();
        $id_peserta_arr = json_decode($ujian->id_peserta_json, TRUE);
        $peserta = mPeserta::whereIn('id_peserta', $id_peserta_arr)->orderBy('pst_nama', 'ASC')->get();
        $bidang = mBidang::where('id_bidang', $ujian->id_bidang)->first();
        $bidang_sub = mBidangSub::where('id_bidang_sub', $ujian->id_bidang_sub)->first();

        $soal_kategori = mSoalKategori
            ::with([
                'soal_section',
                'soal_section.soal',
            ])
            ->where('id_bidang_sub', $ujian->id_bidang_sub)
            ->orderBy('id_soal_kategori', 'ASC')
            ->get();


        /**
         * Digunakan untuk memproses penampilan formula kalkulasi nilai
         */
        $formula_nilai = 'Total Nilai = (';
        $kategori_count = count($soal_kategori) - 1;
        $jumlah_pembagi = 0;
        foreach ($soal_kategori as $key_kategori => $row_soal_kategori) {
            $formula_nilai .= '(' . $row_soal_kategori->skg_nilai . ' x ' . Main::number_to_alphabet($key_kategori) . ') ';

            if ($key_kategori < $kategori_count) {
                $formula_nilai .= ' + ';
            }

            $skg_nilai = $row_soal_kategori->skg_nilai;
            $jumlah_soal = 0;

            /**
             * Digunakan untuk mencari jumlah soal per kategori soal
             */
            foreach ($row_soal_kategori->soal_section as $row_section) {
                foreach ($row_section->soal as $key => $row_soal) {
//                    echo json_encode($row_soal);
//                    echo '<br />';
                    $jumlah_soal++;
                }
            }

            /**
             * proses penjumlahan total soal dikali 2 (dari nilai maksimal) lalu dikali persentase nilai
             */
            $jumlah_pembagi += $skg_nilai * $jumlah_soal;

//            echo $skg_nilai.' * '.$jumlah_soal.' * 2 <br />';


        }

        $pembagi = $jumlah_pembagi;

        $formula_nilai .= ' x 100) / ' . $pembagi;


        $data = [
            'peserta' => $peserta,
            'bidang' => $bidang,
            'bidang_sub' => $bidang_sub,
            'soal_kategori' => $soal_kategori,
            'formula_nilai' => $formula_nilai,
            'ujian' => $ujian
        ];

        return view('ujian/ujian/ujianPaper', $data);
    }

    /**
     *
     * Digunakan untuk proses penyimpan hasil nilai dari mahasiswa ke sistem,
     * dengan multiple mahasiswa
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|\Response
     */
    function paper_insert(Request $request)
    {
        $id_bidang = $request->input('id_bidang');
        $id_bidang_sub = $request->input('id_bidang_sub');
        $id_peserta_arr = $request->input('id_peserta');
        $id_ujian = $request->input('id_ujian');
        $jawaban_arr = $request->input('jawaban');
        $soal_essay_arr = $request->input('soal_essay');
        $ujn_catatan = $request->input('ujn_catatan');

        $member = Session::get('member');
        $id_member = $member->id_member;

        $data = [];
        $id_soal_essay_arr = [];
        $kalkulasi = [];
        $total_pembagi = 0;

        $peserta_nilai = [];
        $peserta_nilai_final = [];


        DB::beginTransaction();
        try {

            foreach ($jawaban_arr as $id_soal_kategori => $soal_kategori) {

                /**
                 * Proses untuk mendapatkan kategori tipe dan persentasi kategori
                 */
                $skg_tipe = mSoalKategori::where('id_soal_kategori', $id_soal_kategori)->value('skg_tipe');
                $skg_nilai = mSoalKategori::where('id_soal_kategori', $id_soal_kategori)->value('skg_nilai');

                /**
                 * Proses untuk memasukkan data kedalam variable agar bisa di foreach selanjutnya
                 */
                $kalkulasi[$id_soal_kategori]['jumlah_soal'] = 0;
                $kalkulasi[$id_soal_kategori]['persentase_kategori'] = $skg_nilai;

                /**
                 * Inisialisasi awal data peserta ke dalam array untuk proses perhitungan nilai per peserta
                 */
                foreach ($id_peserta_arr as $key => $id_peserta) {
                    $kalkulasi[$id_soal_kategori]['peserta'][$id_peserta] = 0;
                }

                $data[] = $skg_tipe;


                foreach ($soal_kategori as $id_soal_section => $soal_section) {

                    foreach ($soal_section as $id_soal => $soal) {

                        /**
                         * Jika tipe kategori soal adalah diisi penguji, maka insert soal yang dikettikan oleh penguji
                         */
                        if ($skg_tipe == 'diisi_penguji') {
                            $soal_essay_data = [
                                'id_ujian' => $id_ujian,
                                'id_soal' => $id_soal,
                                'sey_isi' => $soal_essay_arr[$id_soal_kategori][$id_soal_section][$id_soal]
                            ];
                            $soal_essay_response = mSoalEssay::create($soal_essay_data);
                            $id_soal_essay = $soal_essay_response->id_soal_essay;
                        } else {
                            $id_soal_essay = '';
                        }

                        /**
                         * untuk mendapatkan jumlah soal untuk proses kalkulasi
                         */
                        $kalkulasi[$id_soal_kategori]['jumlah_soal']++;


                        $id_soal_essay_arr[] = $id_soal_essay;

                        foreach ($soal as $id_peserta => $pjw_nilai) {

                            $kalkulasi[$id_soal_kategori]['peserta'][$id_peserta] += $pjw_nilai;

                            /**
                             * Proses simpan jawaban peserta
                             */
                            $data_insert = [
                                'id_ujian' => $id_ujian,
                                'id_member' => $id_member,
                                'id_peserta' => $id_peserta,
                                'id_bidang' => $id_bidang,
                                'id_bidang_sub' => $id_bidang_sub,
                                'id_soal' => $id_soal,
                                'id_soal_essay' => $id_soal_essay,
                                'pjw_nilai' => $pjw_nilai,
                            ];

                            mPesertaJawaban::create($data_insert);
                        }
                    }
                }
            }

            /**
             * inisialisasi awal peserta untuk proses nilai akhir per id peserta
             */
            foreach ($id_peserta_arr as $id_peserta) {
                $peserta_nilai[$id_peserta] = 0;
            }

            /**
             * Proses menjalankan formula agar mendapatkan nilai peserta, dengan formula :
             * Total Nilai = ((3 x A[total_nilai_perkategori]) +
             *               (5 x B[total_nilai_perkategori]) +
             *               (2 x C[total_nilai_perkategori]) x 100) /
             *               566[jumlah_soal]
             */
            foreach ($kalkulasi as $id_soal_kategori => $kalkulasi_row) {
                $jumlah_soal = $kalkulasi_row['jumlah_soal'];
                $persentase_kategori = $kalkulasi_row['persentase_kategori'];

                $total_pembagi += $persentase_kategori * ($jumlah_soal * 2);

                foreach ($kalkulasi_row['peserta'] as $id_peserta => $nilai) {
                    $peserta_nilai[$id_peserta] += ($persentase_kategori * $nilai);
                }
            }

            /**
             * proses nilai final per peserta
             */
            foreach ($peserta_nilai as $id_peserta => $total_nilai) {
                $peserta_nilai_final[$id_peserta] = ($total_nilai * 100) / $total_pembagi;
            }

            /**
             * proses memasukkan nilai ke data per peserta
             */
            foreach ($peserta_nilai_final as $id_peserta => $nilai_akhir) {
                $peserta_nilai_akhir_data = [
                    'id_ujian' => $id_ujian,
                    'id_peserta' => $id_peserta,
                    'id_member' => $id_member,
                    'pna_nilai' => $nilai_akhir,
                    'pna_label' => Main::peserta_lulus_status($nilai_akhir)
                ];

                mPesertaNilaiAkhir::create($peserta_nilai_akhir_data);
            }


            $ujian_data = [
                'ujn_tanggal' => date('Y-m-d'),
                'ujn_catatan' => $ujn_catatan
            ];

            mUjian::where('id_ujian', $id_ujian)->update($ujian_data);

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

        return [
            'redirect' => route('ujianFinish', ['id_ujian' => Main::encrypt($id_ujian)])
        ];


//        return response([$kalkulasi, $peserta_nilai, $total_pembagi, $peserta_nilai_final], 401);
    }

    function finish($id_ujian)
    {
        $id_ujian = Main::decrypt($id_ujian);
        $ujian = mUjian
            ::with([
                'bidang',
                'bidang_sub',
                'peserta_nilai_akhir',
                'peserta_nilai_akhir.peserta'
            ])
            ->where('id_ujian', $id_ujian)
            ->first();

        $data = [
            'ujian' => $ujian
        ];


        return view('ujian/ujian/ujianFinish', $data);
    }

    function delete($id_ujian)
    {
        $id_ujian = Main::decrypt($id_ujian);
        mUjian::where('id_ujian', $id_ujian)->delete();
    }

    function download($id_ujian)
    {
        $id_ujian = Main::decrypt($id_ujian);
        $ujian = mUjian::where('id_ujian', $id_ujian)->first();
        $id_peserta_arr = json_decode($ujian->id_peserta_json, TRUE);
        $peserta = mPeserta::whereIn('id_peserta', $id_peserta_arr)->orderBy('pst_nama', 'ASC')->get();
        $bidang = mBidang::where('id_bidang', $ujian->id_bidang)->first();
        $bidang_sub = mBidangSub::where('id_bidang_sub', $ujian->id_bidang_sub)->first();
        $member = mMember::where('id_member', $ujian->id_member)->first();

        $soal_kategori = mSoalKategori
            ::with([
                'soal_section',
                'soal_section.soal',
            ])
            ->where([
                'id_bidang' => $ujian->id_bidang,
                'id_bidang_sub' => $ujian->id_bidang_sub
            ])
            ->orderBy('id_soal_kategori', 'ASC')
            ->get();

        $data = [
            'member' => $member,
            'ujian' => $ujian,
            'peserta' => $peserta,
            'bidang' => $bidang,
            'bidang_sub' => $bidang_sub,
            'soal_kategori' => $soal_kategori
        ];

//        return view('ujian/ujian/ujianDownload', $data);

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('ujian/ujian/ujianDownload', $data);

        return $pdf->download('Ujian Praktik Bidan '.date('d F Y', strtotime($ujian->created_at)));

    }
}
