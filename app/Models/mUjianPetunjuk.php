<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mUjianPetunjuk extends Model
{
    use SoftDeletes;

    protected $table = 'ujian_petunjuk';
    protected $primaryKey = 'id_ujian_petunjuk';
    protected $fillable = [
        'upj_judul',
        'upj_keterangan'
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
