<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPesertaNilaiAkhir extends Model
{
    use SoftDeletes;

    protected $table = 'peserta_nilai_akhir';
    protected $primaryKey = 'id_peserta_nilai_akhir';
    protected $fillable = [
        'id_peserta_nilai_akhir',
        'id_ujian',
        'id_peserta',
        'id_member',
        'pna_nilai',
        'pna_label',
    ];

    public function peserta() {
        return $this->belongsTo(mPeserta::class, 'id_peserta');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
