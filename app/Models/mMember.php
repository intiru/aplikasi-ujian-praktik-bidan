<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mMember extends Model
{
    use SoftDeletes;

    protected $table = 'member';
    protected $primaryKey = 'id_member';
    protected $fillable = [
        'mbr_nama',
        'mbr_email',
        'mbr_password',
        'mbr_tanggal_lahir',
        'mbr_alamat',
        'mbr_pekerjaan',
        'mbr_nidn',
        'mbr_prodi',
        'mbr_nim',
        'mbr_phone',
        'mbr_status',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
