<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mUjian extends Model
{
    use SoftDeletes;

    protected $table = 'ujian';
    protected $primaryKey = 'id_ujian';
    protected $fillable = [
        'id_member',
        'id_petunjuk_ujian',
        'id_bidang',
        'id_bidang_sub',
        'id_peserta_json',
        'peserta_nilai_json',
        'ujn_judul',
        'ujn_tanggal',
        'ujn_catatan'
    ];

    public function bidang()
    {
        return $this->belongsTo(mBidang::class, 'id_bidang');
    }

    public function bidang_sub()
    {
        return $this->belongsTo(mBidangSub::class, 'id_bidang_sub');
    }

    public function peserta_nilai_akhir()
    {
        return $this->hasMany(mPesertaNilaiAkhir::class, 'id_ujian');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
