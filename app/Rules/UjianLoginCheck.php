<?php

namespace app\Rules;

use app\Models\mMember;
use app\Models\mUser;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use app\Models\mUserRole;
use Illuminate\Support\Facades\Session;

class UjianLoginCheck implements Rule
{

    protected $email;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email= $email;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $email = $this->email;
        $password = $value;
        $userPassword = mMember::where('mbr_email', $email)->value('mbr_password');
        $status = FALSE;

        /**
         * Login user
         */
        if (Hash::check($password, $userPassword)) {
            $status = TRUE;
        }

        if ($status) {
            $member = mMember
                ::where([
                    'mbr_email' => $email,
                ])
                ->first();

            $session = [
                'login_member' => TRUE,
                'member' => $member
            ];

            Session::put($session);
        }


        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Email atau Password tidak benar';
    }
}
