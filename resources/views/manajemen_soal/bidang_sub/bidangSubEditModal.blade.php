<form action="{{ route('bidangSubUpdate', ['id_bidang_sub'=>Main::encrypt($edit->id_bidang_sub)]) }}" method="post" class="m-form form-send">
    {{ csrf_field() }}
    <div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group">
                        <label class="form-control-label required">Nama Keterampilan</label>
                        <select class="form-control" name="id_bidang">
                            @foreach($bidang as $row)
                                <option value="{{ $row->id_bidang }}" {{ $row->id_bidang == $edit->id_bidang ? 'selected':'' }}>{{ $row->bdg_nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group m-form__group">
                        <label class="form-control-label required">Nama Perasat</label>
                        <input type="text" class="form-control m-input" name="bds_nama" value="{{ $edit->bds_nama }}"
                               autofocus>
                    </div>
                    <div class="form-group m-form__group">
                        <label class="form-control-label">Keterangan Perasat</label>
                        <textarea class="form-control" name="bds_keterangan">{{ $edit->bds_keterangan }}</textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-update btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>