@extends('ujian/general/index')

@section('body')
    <div class="app-body">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2>Ujian Edit</h2>
                </div>
                <div class="col-12">
                    <br />
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Pilih Keterampilan :</label>
                        <div class="col-sm-10 bidang">
                            <button class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modal-bidang">Browse & Pilih</button>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Pilih Perasat :</label>
                        <div class="col-sm-10 bidang">
                            <button class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modal-bidang-sub">Browse & Pilih</button>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Mahasiswa :</label>
                        <div class="col-sm-10">
                            <button class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modal-mahasiswa">Browse & Pilih</button>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Petunjuk :</label>
                        <div class="col-sm-10">
                            <button class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modal-petunjuk">Browse & Pilih</button>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <div class="col-sm-12 text-center">
                            <hr />
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <div class="col-sm-12 text-center">
                            <button class="btn btn-primary">Perbarui</button>
                            <a href="{{ route('ujianList') }}" class="btn btn-warning">Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modal-mahasiswa" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Pilih Mahasiswa</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body modal-body-list-item">
                    <div class="row">
                        <label class="form-check-label col-10" for="mahasiswa-1">
                            I Putu Mahendra Adi Wardana
                        </label>
                        <div class="col-2">
                            <input class="form-check-input" type="checkbox" value="" id="mahasiswa-1">
                        </div>
                    </div>
                    <div class="row">
                        <label class="form-check-label col-10" for="mahasiswa-2">
                            Etick Pristyan Dewi
                        </label>
                        <div class="col-2">
                            <input class="form-check-input" type="checkbox" value="" id="mahasiswa-2">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-bidang" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Pilih Keterampilan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body modal-body-list-item">
                    <div class="row">
                        <label class="form-check-label col-10" for="bidang-1">
                            Fisiologis
                        </label>
                        <div class="col-2">
                            <input class="form-check-input" type="checkbox" value="" id="bidang-1">
                        </div>
                    </div>
                    <div class="row">
                        <label class="form-check-label col-10" for="bidang-2">
                            Patologis
                        </label>
                        <div class="col-2">
                            <input class="form-check-input" type="checkbox" value="" id="bidang-2">
                        </div>
                    </div>
                    <div class="row">
                        <label class="form-check-label col-10" for="bidang-3">
                            KB
                        </label>
                        <div class="col-2">
                            <input class="form-check-input" type="checkbox" value="" id="bidang-3">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-bidang-sub" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Pilih Perasat</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body modal-body-list-item">
                    <div class="row">
                        <label class="form-check-label col-10" for="bidang-sub-1">
                            ANC
                        </label>
                        <div class="col-2">
                            <input class="form-check-input" type="checkbox" value="" id="bidang-sub-1">
                        </div>
                    </div>
                    <div class="row">
                        <label class="form-check-label col-10" for="bidang-sub-2">
                            INC
                        </label>
                        <div class="col-2">
                            <input class="form-check-input" type="checkbox" value="" id="bidang-sub-2" checked>
                        </div>
                    </div>
                    <div class="row">
                        <label class="form-check-label col-10" for="bidang-sub-3">
                            PNC
                        </label>
                        <div class="col-2">
                            <input class="form-check-input" type="checkbox" value="" id="bidang-sub-3" checked>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-petunjuk" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Pilih Petunjuk</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body modal-body-list-item">
                    <div class="row">
                        <label class="form-check-label col-10" for="petunjuk-1">
                            <i class="fa fa-info-circle"></i> Petunjuk 1
                        </label>
                        <div class="col-2">
                            <input class="form-check-input" type="radio" name="petunjuk" value="" id="petunjuk-1">
                        </div>
                    </div>
                    <div class="row">
                        <label class="form-check-label col-10" for="petunjuk-2">
                            <i class="fa fa-info-circle"></i> Petunjuk 2
                        </label>
                        <div class="col-2">
                            <input class="form-check-input" type="radio"  name="petunjuk"value="" id="petunjuk-2">
                        </div>
                    </div>
                    <div class="row">
                        <label class="form-check-label col-10" for="petunjuk-3">
                            <i class="fa fa-info-circle"></i> Petunjuk 3
                        </label>
                        <div class="col-2">
                            <input class="form-check-input" type="radio" name="petunjuk" value="" id="petunjuk-3" checked>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    @include('ujian.component.navigation')
@endsection