@extends('ujian/general/index')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugin/datepicker/css/datepicker.css') }}">
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('plugin/datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.datepicker').datepicker({
                autoclose: true
            });
        });
    </script>
@endsection

@section('body')
    <form action="{{ route('profileUpdate') }}"
          method="post"
          class="m-form form-send  needs-validation"
          autocomplete="off"
          data-redirect="{{ route('ujianLogin') }}">
        {{ csrf_field() }}
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <br/>
                    <h2>Edit Profile</h2>
                </div>
                <div class="col-12">
                    <br/>
                    <br/>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mbr_nama" value="{{ $member->mbr_nama }}">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Tanggal Lahir</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control datepicker" value="{{ date('d-m-Y', strtotime($member->mbr_tanggal_lahir)) }}" data-date-format="dd-mm-yyyy" name="mbr_tanggal_lahir" readonly="">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Pekerjaan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mbr_pekerjaan" value="{{ $member->mbr_pekerjaan }}">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Alamat</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mbr_alamat" value="{{ $member->mbr_alamat }}">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="mbr_email" value="{{ $member->mbr_email }}">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="mbr_password">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-primary">Update Profile</button>
                            <a href="{{ route('ujianList') }}" class="btn btn-warning">Kembali</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 text-center register-footer">
                    <br/>
                    <img src="{{ asset('images/logo-ujian-praktik-bidan-row.png') }}" class="img-responsive">
                </div>
            </div>
        </div>
    </form>

    <br/>
    <br/>
    <br/>

    @include('ujian.component.navigation', $navigation)
@endsection