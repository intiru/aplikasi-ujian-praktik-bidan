@extends('ujian/general/index')

@section('body')
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <br />
                <h2>Tentang Aplikasi</h2>
            </div>
            <div class="col-12">
                <br />
                <br />
                <p align="center">Penilaian Praktik Kebidanan untuk memudahkan penguji maupun mahasiswa dalam mencoba menilai praktik ujian dengan daftar tilik</p>
            </div>
            <div class="col-12 text-center register-footer">
                <br />
                <img src="{{ asset('images/logo-ujian-praktik-bidan-row.png') }}" class="img-responsive">
            </div>
        </div>
    </div>

    @include('ujian.component.navigation', $navigation)

@endsection